<footer class="text-muted py-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-10">
                    <p><b>FairSocialNet</b> s'engage pour des <b>services</b> Internet <b>éthiques</b>, sans <b>publicité</b>, fonctionnant de manière <b>transparente</b> avec des <b>logiciels libres</b> exclusivement.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-10">
                    <p>Association à <b>but non lucratif</b> depuis <b>2018</b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-10">
                    <p class="text-muted">Thème réalisé par nos mains à l'aide de <a href="/">Bootstrap</a>, avec un effort particulier sur l'accessibilité.</p>
                </div>
            </div>
            <p class="float-end mb-1"><a href="#">Retour en haut</a></p>
        </div>
    </footer>
    <script src="<?php get_website_url(); ?>libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="<?php get_website_url(); ?>libs/bootstrap/5.0.1/js/bootstrap.bundle.min.js"></script>
    <script src="<?php get_website_url(); ?>libs/simpleParallax/simpleParallax.min.js"></script>
    <script src="<?php get_website_url(); ?>scripts.js"></script>
    <script src="<?php get_website_url(); ?>scripts/fsn.js"></script>
    <script src="<?php get_website_url(); ?>scripts/fsn/theme.js"></script>
    <script src="<?php get_website_url(); ?>scripts/fsn/menu.js"></script>
</body>
</html>